/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/sw.js":
/*!****************************!*\
  !*** ./resources/js/sw.js ***!
  \****************************/
/***/ (function() {

var _this = this;

var staticCacheName = "pwa-v" + new Date().getTime();
var filesToCache = [//    '/css/app.css',
  //    '/js/app.js'
]; // Cache on install

self.addEventListener("install", function (event) {
  _this.skipWaiting();

  event.waitUntil(caches.open(staticCacheName).then(function (cache) {
    return cache.addAll(filesToCache);
  }));
}); // Clear cache on activate

self.addEventListener('activate', function (event) {
  event.waitUntil(caches.keys().then(function (cacheNames) {
    return Promise.all(cacheNames.filter(function (cacheName) {
      return cacheName.startsWith("pwa-");
    }).filter(function (cacheName) {
      return cacheName !== staticCacheName;
    }).map(function (cacheName) {
      return caches["delete"](cacheName);
    }));
  }));
}); // Serve from Cache

self.addEventListener("fetch", function (event) {
  event.respondWith(caches.match(event.request).then(function (response) {
    return response || fetch(event.request);
  })["catch"](function () {
    return caches.match('offline');
  }));
});

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/sw.js"]();
/******/ 	
/******/ })()
;