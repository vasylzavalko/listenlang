<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminCategory extends Model
{

    use HasFactory;
    protected $table = 'categories';
    protected $fillable = ['title', 'slug', 'parent_id', 'status', 'image'];

    public static function tree()
    {
        $category = AdminCategory::where('status',1)->orderBy('sort', 'asc')->get();
        $temp = [];
        foreach ($category as $node) {
            $temp[$node['id']] = [
                'id' => $node['id'],
                'parent_id' => $node['parent_id'],
                'title' => $node['title'],
                'slug' => $node['slug'],
            ];
        }
        $category = $temp;

        $tree = [];
        foreach ($category as &$node) {
            if (!$node['parent_id']){
                $tree[$node['slug']] = &$node;
            }else{
                $category[$node['parent_id']]['childs'][$node['slug']] = &$node;
            }
        }

        $return = $tree;
        return $return;
    }

    public static function parentCategory(int $category ){
        $return = [];
        $end = 0;
        while($end == 0){
            $categories = AdminCategory::where('id', $category)->first();
            $category = $categories->parent_id;
            if($category==0){
                $end=1;
            }else{
                $return[] = $category;
            }
        }
        $return = array_reverse($return);
        return $return;
    }

    public static function updateData(int $id, array $request, $files)
    {
        $data = [
            'title' => $request['title'],
            'slug' => $request['slug'],
            'status' => $request['status'],
        ];


        if ($files) {
            $dir = "/category/";
            if (file_exists($dir . AdminCategory::find($id)->image)) {
                unlink($dir . AdminCategory::find($id)->image);
            }
            $imgName =  "category_" . $id . "." . $files->getClientOriginalExtension();
            $files->storeAs( $dir, $imgName, ['disk' => 'public_files'] );
            $data['image'] = $imgName;
        }
        return AdminCategory::where('id', $id)->update($data);
    }

    public function titles()
    {
        return $this->hasMany(AdminCategoryTitles::class, 'category_id');
    }

    public function expressions()
    {
        return $this->hasMany(AdminCategoryExpressions::class, 'category_id');
    }

    public static function getTitles($id)
    {
        $return = [];
        $titles = AdminCategory::find($id)->titles;
        foreach ($titles as $value){
            $return[$value['lang_id']] = $value;
        }
        return $return;
    }

}
