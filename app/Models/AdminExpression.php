<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\AdminCategory as CategoryModel;
use App\Models\AdminCategoryExpressions as CategoryExpressionModel;

class AdminExpression extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'expressions';
    protected $fillable = ['rating', 'views', 'status', 'title'];

    public static function expressions($request)
    {
        $categoryId = ($request->query('c') && is_numeric($request->query('c')))?$request->query('c'):null;
        $builder = AdminExpression::where('status', '>=', 0);
        if($categoryId){
            $builder = $builder->join('category_expressions', 'category_expressions.expression_id', 'expressions.id');
            $builder = $builder->where('category_expressions.category_id', $categoryId);
        }
        $builder = $builder->orderBy('expressions.id', 'DESC');
        return $builder->paginate(20);
    }

    public static function updateData(int $id, array $request)
    {
        return AdminExpression::where('id', $id)
            ->update([
                'title' => $request['title'],
                'status' => $request['status'],
            ]);
    }

    public static function updateCategory(int $id, array $category)
    {
        CategoryExpressionModel::where('expression_id', $id)->delete();

        if(count($category)>0) {
            $categories = [];
            foreach ($category as $value) {
                $parentCategory = CategoryModel::parentCategory($value);
                $categories = array_merge($categories, $parentCategory);
            }
            $category = array_merge($category, $categories);
            $category = array_unique($category);
            foreach ($category as $value) {
                CategoryExpressionModel::create([
                    'expression_id' => $id,
                    'category_id' => $value,
                ]);
            }
        }
        return $category;
    }

    public function titles()
    {
        return $this->hasMany(AdminExpressionTitles::class, 'expression_id');
    }

    public static function getTitles($id)
    {
        $return = [];
        $titles = AdminExpression::find($id)->titles;
        foreach ($titles as $value){
            $return[$value['lang_id']] = $value;
        }
        return $return;
    }

    public static function getTags($id)
    {
        $return = [];
        $titles = AdminExpression::find($id)->titles;
        foreach ($titles as $value){
            $strBouble = [];
            $strTriple = [];
            $str = $value['title'];
            $strSingle = explode(" ", $str);
            $temp = [];
            foreach ($strSingle as $valueStr){
                $str = trim($valueStr,' ');
                $str = trim($str,',');
                $str = trim($str,'.');
                $str = trim($str,'!');
                $str = trim($str,'?');
                $temp[] = $str;
            }
            $strSingle = $temp;
            for ($i=0;$i<count($strSingle)-1;$i++) {
                $strBouble[] = $strSingle[$i]." ".$strSingle[$i+1];
            }
            for ($i=0;$i<count($strSingle)-2;$i++) {
                $strTriple[] = $strSingle[$i]." ".$strSingle[$i+1]." ".$strSingle[$i+2];
            }
            $return[$value['lang_id']] = array_merge($strSingle, $strBouble, $strTriple);
        }
        return $return;
    }


}
