<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\AdminCategory as CategoryModel;


class Front extends Model
{

    static function category()
    {
        $localeId = Front::localeId();
        $category = CategoryModel::select('categories.id', 'categories.parent_id', 'categories.slug', 'categories.image', 'category_titles.title')
            ->join('category_titles', 'category_titles.category_id', 'categories.id')
            ->where('categories.status', 1)
            ->where('category_titles.lang_id',$localeId)
            ->orderBy('categories.sort', 'asc')
            ->get();
        $temp = [];
        foreach ($category as $node) {
            $temp[$node->id] = [
                'id' => $node->id,
                'parent_id' => $node->parent_id,
                'title' => $node->title,
                'slug' => $node->slug,
                'image' => "files/category/".$node->image,
                'expressions' => $node->expressions->count(),
            ];
        }
        return $temp;
    }

    static function categoryTree()
    {
        $tree = [];
        $category = Front::category();
        foreach ($category as &$node) {
            if(isset($node['parent_id'])){
                if (!$node['parent_id']){
                    $tree[$node['slug']] = &$node;
                }else{
                    $category[$node['parent_id']]['childs'][$node['slug']] = &$node;
                }
            }
        }
        return $tree;
    }

    static function getCategory($id)
    {
        $localeId = Front::localeId();
        $builder = DB::table('categories');
        $builder = $builder->select('categories.id', 'categories.parent_id', 'categories.slug', 'categories.image', 'category_titles.title');
        $builder = $builder->where('categories.status',1);
        $builder = $builder->where('categories.id',$id);
        $builder = $builder->join('category_titles', 'category_titles.category_id', 'categories.id');
        $builder = $builder->where('category_titles.lang_id',$localeId);
        $category = $builder->get();
        $temp = [];
        foreach ($category as $node) {
            $temp = [
                'id' => $node->id,
                'parent_id' => $node->parent_id,
                'title' => $node->title,
                'slug' => $node->slug,
                'image' => "files/category/".$node->image,
                'expressions' => 0,
            ];
        }
        return $temp;
    }

    static function expressions($id)
    {
        $return = [];
        $from = Session('from');
        $to = Session('to');

        $builder = DB::table('category_expressions')
            ->select('expressions.id as id', 'expressions.rating', 'expressions.views', 'from.title as title_from', 'from.voice as voice_from', 'to.title as title_to', 'to.voice as voice_to')
            ->where('category_expressions.category_id', $id)
            ->where('expressions.status', 1)
            ->join('expressions', 'expressions.id', 'category_expressions.expression_id')
            ->join('expression_titles as from', 'from.expression_id','expressions.id')
            ->where('from.lang_id', $from)
            ->join('expression_titles as to', 'to.expression_id','expressions.id')
            ->where('to.lang_id', $to);

        $expressions = $builder->inRandomOrder()->get();
        foreach ($expressions as $node) {
            $return[$node->id] = [
                'id' => $node->id,
                'rating' => $node->rating,
                'views' => $node->views,
                'from_id' => $from+0,
                'from_title' => $node->title_from,
                'from_voice' => $node->voice_from,
                'to_id' => $to,
                'to_title' => $node->title_to,
                'to_voice' => $node->voice_to,
            ];
        }
        return $return;
    }

    static function saveStatistic($categoryId)
    {
        $user = Auth::user();
        if ($user) {
            DB::table('user_stats')->where('id', $user->id)->insert([
                'user_id' => $user->id,
                'category_id' => $categoryId,
                'tag_id' => 0,
                'langfrom_id' => session('from'),
                'langto_id' => session('to'),
            ]);
        }
    }

    static function langs()
    {
        $langs = DB::table('langs')
            ->where('status', 1)
            ->orderBy('sort', 'ASC')
            ->get();
        $return = [];
        foreach ($langs as $node) {
            $return[$node->id] = [
                'id' => $node->id,
                'title' => $node->title,
                'code' => $node->code,
                'voice_code' => $node->voice_code,
                'voice_name' => $node->voice_name,
            ];
        }
        return $return;
    }

    static function locale()
    {
        return ( session('locale') )?session('locale'):config('app.fallback_locale');
    }

    static function localeId()
    {
        return DB::table('langs')->where('code', Front::locale())->first()->id;
    }

}
