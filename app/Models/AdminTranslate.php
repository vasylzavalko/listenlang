<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AdminLang as LangModel;

class AdminTranslate extends Model
{

    static function traslateStr($str, $langs)
    {
        $return = [];
        foreach ($langs as $key => $value) {
            if (config('google.status')==1) {
                $query = '&q=' . rawurlencode($str) . '&target=' . $value->code;
                $string = AdminTranslate::translate($query);
            } else {
                $string = "Google Translate Close";
            }
            $return[$value->id] = $string;
        }
        return $return;
    }

    static function strToVoice($expression_id, $lang_id, $str)
    {

        if (config('google.status')==1) {

            $lang = LangModel::find($lang_id);
            $voiceCode = $lang->voice_code;
            $voiceName = $lang->voice_name;

            $dir = "files/voice/".$expression_id;
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            $voice = AdminTranslate::textToSpeech($str, $voiceCode, $voiceName);
            if($voice!=null){
                $mp3 = "voice-".$expression_id.$lang_id.".mp3";
                file_put_contents($dir."/".$mp3, $voice);
                return $mp3;
            }

        }
        return 0;
    }

    static function mbUcfirst($str, $encoding='UTF-8')
    {
        $str = mb_ereg_replace('^[\ ]+', '', $str);
        $str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding).mb_substr($str, 1, mb_strlen($str), $encoding);
        return $str;
    }

    static function translate($query)
    {
        $apiKey = config('google.apiKey');
        $apiUrl = config('google.apiUrlTranslate');
        $url = $apiUrl.'?key=' . $apiKey . $query;
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($handle);
        $response = json_decode($response, true);
        $responseCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);
        if ($responseCode != 200) {
            $string = "Error";
        } else {
            $string = $response['data']['translations'][0]['translatedText'];
        }
        return AdminTranslate::mbUcfirst($string);
    }

    static function textToSpeech($str, $voiceCode, $voiceName)
    {

        $data = [
            'audioConfig' => [
                'audioEncoding' => 'LINEAR16',
                'pitch' => 0,
                'speakingRate' => config('google.speakingRate1')
            ],
            'input' => [
                'text' => $str
            ],
            'voice' => [
                'languageCode' => $voiceCode,
                'name' => $voiceName,
            ],
        ];
        $data = json_encode($data);
        $url = config('google.apiUrlTextToSpeech').'?fields=audioContent&key=' . config('google.apiKey');
        $handle = curl_init($url);

        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            ]
        );
        $response = curl_exec($handle);
        $responseDecoded = json_decode($response, true);
        curl_close($handle);

        if ($responseDecoded['audioContent']) {
            return base64_decode($responseDecoded['audioContent']);
        }
        return null;
    }
}
