<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminCategoryTitles extends Model
{
    use HasFactory;
    protected $table = 'category_titles';
    protected $fillable = ['category_id', 'lang_id', 'title'];
    public $timestamps = false;

    public static function updateData($category_id, array $request)
    {
        foreach ($request as $key=>$value){
            $valueArray = explode("_", $key);
            if(isset($valueArray[1]) AND is_numeric($valueArray[1])){
                AdminCategoryTitles::updateOrCreate(
                    [
                        'category_id' => $category_id,
                        'lang_id' => $valueArray[1],
                    ],
                    [
                        'category_id' => $category_id,
                        'lang_id' => $valueArray[1],
                        'title' => $value,
                    ]);
            }
        }
    }

}
