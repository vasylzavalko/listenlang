<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AdminAction extends Model
{

    static function sorting($table, $sort)
    {
        if( !empty($table) && !empty($sort) ){
            $sort = explode("|", $sort);
            if(count($sort)>0){
                $count = 0;
                foreach ($sort as $id){
                    DB::table($table)->where('id', $id)->update(['sort' => $count]);
                    $count++;
                }
                return true;
            }
        }
        return false;
    }

    static function tag($expressionId, $langId, $tag, $action)
    {
        $tableTag = "tags";
        $tableTagExpression = "tag_expressions";
        $isTag = DB::table($tableTag)->where('title', $tag)->where('lang_id', $langId)->first();
        $tagId = ($isTag!=null)?$isTag->id:0;
        if($tagId==0){
            $tagId = DB::table($tableTag)->insertGetId([
                'title' => $tag,
                'lang_id' => $langId,
            ]);
        }
        if($action=='add' && $tagId>0){
            DB::table($tableTagExpression)->insert([
                'tag_id' => $tagId,
                'expression_id' => $expressionId,
            ]);
            return true;
        }
        if($action=='delete' && $tagId>0){
            DB::table($tableTagExpression)->where('tag_id', $tagId)->where('expression_id', $expressionId)->delete();
            $isTagExpression = DB::table($tableTagExpression)->where('tag_id', $tagId)->first();
            if(($isTagExpression==null)){
                DB::table($tableTag)->where('id', $tagId)->delete();
            }
            return true;
        }
        return false;
    }

    static function deleteDir($dir)
    {
        if (file_exists($dir)) {
            array_map('unlink', glob("$dir/*.*"));
            rmdir($dir);
        }
    }

    static function deleteCategoryImage($id)
    {
        $table = 'categories';
        if($image = DB::table($table)->where('id', $id)->first()->image){
            DB::table($table)->where('id', $id)->update(['image' => 'none']);
            Storage::disk('public_files')->delete('/category/'.$image);
            return true;
        }
        return false;
    }

    static function selectLang($id, $type)
    {
        session([$type => $id]);
        if($type=='from'){
            $to = DB::table('langs')->where('id', '!=', $id)->first()->id;
            session(['to' => $to]);
        }
        return $type."-".$id;
    }

}
