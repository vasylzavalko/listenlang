<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminLang extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'langs';
    protected $fillable = ['title','code', 'voice_code', 'voice_name'];

    public static function updateData(int $id, array $request)
    {
        return AdminLang::where('id', $id)
            ->update($request);
    }

    public static function updateDataDestroy(int $id)
    {
        $lang = AdminLang::find($id);
        AdminLang::where('id', $id)
            ->update([
                'title' => $lang->title." DELETED",
                'code' => $lang->code." DELETED",
            ]);
    }

}
