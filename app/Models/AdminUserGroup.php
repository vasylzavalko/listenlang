<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminUserGroup extends Model
{
    use HasFactory;
    protected $table = 'user_groups';

    static function getGroup()
    {
        $return = [];
        $group = AdminUserGroup::get();
        foreach ($group as $value){
            $return[$value['id']] = $value['title'];
        }
        return $return;
    }

}
