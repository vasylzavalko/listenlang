<?php

namespace App\Models;

use App\Models\AdminLang as LangModel;
use App\Models\AdminTranslate as TranslateModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminExpressionTitles extends Model
{
    use HasFactory;
    protected $table = 'expression_titles';
    protected $fillable = ['expression_id', 'lang_id', 'voice', 'title'];

    public static function createData($expression_id, $title)
    {
        $langs = LangModel::where('status',1)->orderBy('sort', 'asc')->get();
        $translateTitle = TranslateModel::traslateStr($title, $langs);

        foreach ($langs as $lang){
            // Update Voice
            $voice = TranslateModel::strToVoice($expression_id, $lang['id'], $translateTitle[$lang['id']]);
            AdminExpressionTitles::create(
                [
                    'expression_id' => $expression_id,
                    'lang_id' => $lang['id'],
                    'title' => $translateTitle[$lang['id']],
                    'voice' => $voice,
                ]);
        }

        return $langs;
    }

    public static function updateData($expression_id, array $request)
    {
        foreach ($request as $key=>$value){
            $valueArray = explode("_", $key);
            if(isset($valueArray[1]) AND is_numeric($valueArray[1])){

                // Update Voice
                $voice = TranslateModel::strToVoice($expression_id, $valueArray[1], $value);
                $data = [
                    'expression_id' => $expression_id,
                    'lang_id' => $valueArray[1],
                    'title' => $value,
                ];
                if($voice!=0){
                    $data['voice'] = $voice;
                }
                AdminExpressionTitles::updateOrCreate( [ 'expression_id' => $expression_id, 'lang_id' => $valueArray[1] ], $data );

            }
        }
    }

}
