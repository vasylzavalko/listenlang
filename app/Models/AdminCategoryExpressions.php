<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminCategoryExpressions extends Model
{
    use HasFactory;
    protected $table = 'category_expressions';
    protected $fillable = ['category_id','expression_id'];
    public $timestamps = false;

    public static function category(int $id)
    {
        $return = [];
        $categories = AdminCategoryExpressions::where('expression_id', $id)->get();
        foreach ($categories as $value){
            $return[] = $value->category_id;
        }
        return $return;
    }

}
