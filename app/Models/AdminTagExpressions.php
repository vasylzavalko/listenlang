<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AdminTag as TagModel;

class AdminTagExpressions extends Model
{
    use HasFactory;
    protected $table = 'tag_expressions';
    protected $fillable = ['tag_id','expression_id'];
    public $timestamps = false;

    public static function getTags($id)
    {
        $return = [];
        $tagExpression = AdminTagExpressions::where('expression_id', $id)->get();
        foreach ($tagExpression as $value){
            $return[$value->tag_id] = AdminTagExpressions::find($value->id)->titles->title;;
        }
        return $return;
    }

    public function titles()
    {
        return $this->hasOne(TagModel::class, 'id', 'tag_id');
    }

}
