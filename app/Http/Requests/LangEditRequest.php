<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LangEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required',
                'max:60',
                'string',
                Rule::unique('langs')->ignore($this->route('lang')),
            ],
            'code' => [
                'required',
                'max:6',
                'string',
                Rule::unique('langs')->ignore($this->route('lang')),
            ],
        ];

    }
}
