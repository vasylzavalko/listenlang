<?php


namespace App\Http\Controllers;

use App\Models\AdminTag as TagModel;
use App\Models\AdminLang as LangModel;
use Illuminate\Http\Request;

class AdminTag extends Controller
{

    public function index(Request $request)
    {
        $langs = LangModel::where('status', 1)->orderBy('sort', 'asc')->get();
        $langActive = ($request->query('c'))?$request->query('c'):$langs[0]['id'];
        return view('admin/tags', [
            'tags' =>  TagModel::where('lang_id', $langActive)->paginate(66),
            'langs' => $langs,
            'langActive' => $langActive,
            'languages' => config('app.languages'),
        ]);
    }

}
