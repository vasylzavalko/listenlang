<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdminTranslate as TranslateModel;
use App\Models\AdminLang as LangModel;
use App\Models\AdminCategory as CategoryModel;
use App\Models\AdminCategoryExpressions as CategoryExpressionModel;
use App\Models\AdminExpression as ExpressionModel;
use App\Models\AdminExpressionTitles as ExpressionTitlesModel;
use App\Models\AdminTagExpressions as TagExpressionModel;
use App\Models\AdminAction as ActionModel;
use App\Http\Requests\ExpressionAddRequest;
use App\Http\Requests\ExpressionEditRequest;

class AdminExpression extends Controller
{

    public function index(Request $request)
    {
        return view('admin/expression', [
            'languages' => config('app.languages'),
            'expressions' => ExpressionModel::expressions($request),
            'categoryTree' => CategoryModel::tree(),
            'categoryActive' => ($request->query('c') && is_numeric($request->query('c')))?CategoryModel::find($request->query('c')):null,
        ]);
    }

    /**
     * @param ExpressionAddRequest $request
     * @return
     */
    public function store(ExpressionAddRequest $request)
    {
        $expression = ExpressionModel::create( $request->all() );
        ExpressionTitlesModel::createData( $expression->id, $request['title'] );
        return redirect()->route('admin.expression.index')->with( 'addSuccessful', 'ExpressionAddSuccessful' );
    }

    public function edit($id)
    {
        $expression = ExpressionModel::find($id);
        $langs = LangModel::where('status',1)->orderBy('sort', 'asc')->get();
        $translateTitle = TranslateModel::traslateStr($expression->title, $langs);
        $categoryTree = CategoryModel::tree();
        $category = CategoryExpressionModel::category($id);


        return view('admin/expression_edit', [
            'languages' => config('app.languages'),
            'expression' => $expression,
            'titles' => ExpressionModel::getTitles($id),
            'translateTitle' => $translateTitle,
            'langs' => $langs,
            'categoryTree' => $categoryTree,
            'category' => $category,
            'tagSelect' => ExpressionModel::getTags($id),
            'tags' => TagExpressionModel::getTags($id),
        ]);
    }

    public function update(ExpressionEditRequest $request, $id)
    {
        ExpressionModel::updateData($id, $request->except(['_token', '_method']));
        ExpressionTitlesModel::updateData($id, $request->except(['_token', '_method', 'category', 'title', 'slug', 'status']));
        $category = ($request['category']==null)?[]:$request['category'];
        ExpressionModel::updateCategory($id, $category);

        if($request['submit']=='save-exit'){
            return redirect()->route('admin.expression.index' )->with( 'addSuccessful', 'ExpressionUpdateSuccessful' );
        }
        return redirect()->route('admin.expression.edit', $id )->with( 'addSuccessful', 'ExpressionUpdateSuccessful' );
    }

    public function destroy(Request $request)
    {
        ExpressionModel::destroy($request['id']);
        ExpressionTitlesModel::where('expression_id', $request['id'])->delete();
        CategoryExpressionModel::where('expression_id', $request['id'])->delete();
        TagExpressionModel::where('expression_id', $request['id'])->delete();
        $dir = "files/voice/".$request['id'];
        ActionModel::deleteDir($dir);
        return redirect()->route('admin.expression.index')->with( 'addSuccessful', 'ExpressionDeleteSuccessful' );
    }
}
