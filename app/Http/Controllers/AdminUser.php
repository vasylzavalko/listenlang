<?php

namespace App\Http\Controllers;

use App\Models\User as UserModel;
use App\Models\AdminUserStat as UserStatModel;
use App\Models\AdminUserGroup as UserGroupModel;
use Illuminate\Http\Request;

class AdminUser extends Controller
{

    public function index()
    {
        return view('admin/users', [
            'languages' => config('app.languages'),
            "users" => UserModel::paginate(12),
            "groups" => UserGroupModel::getGroup(),
        ]);
    }

    public function changeGroup(Request $request)
    {
        if($request['id']>1) {
            $user = UserModel::find($request['id']);
            $user->group_id = $request['group'];
            $user->save();
        }
        return redirect()->route('admin.user.index')->with( 'addSuccessful', 'UserUpdateSuccessful' );
    }

    public function destroy(Request $request)
    {
        if($request['id']>1) {
            UserModel::where('id', $request['id'])->delete();
            UserStatModel::where('user_id', $request['id'])->delete();
        }
        return redirect()->route('admin.user.index')->with( 'addSuccessful', 'UserDeleteSuccessful' );
    }

}
