<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdminAction as ActionModel;

class AdminAction extends Controller
{
    public function sorting(Request $request)
    {
        return ActionModel::sorting($request['table'],$request['sort']);
    }

    public function tag(Request $request)
    {
        return ActionModel::tag($request['expression_id'], $request['lang_id'], $request['tag'], $request['action']);
    }

    public function deleteCategoryImage(Request $request)
    {
        return ActionModel::deleteCategoryImage($request['id']);
    }

    public function selectLang(Request $request)
    {
        return ActionModel::selectLang($request['id'], $request['type']);
    }

}
