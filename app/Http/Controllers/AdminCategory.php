<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdminTranslate as TranslateModel;
use App\Models\AdminLang as LangModel;
use App\Models\AdminCategory as CategoryModel;
use App\Models\AdminCategoryTitles as CategoryTitlesModel;
use App\Models\AdminCategoryExpressions as CategoryExpressionModel;
use App\Http\Requests\CategoryAddRequest;
use App\Http\Requests\CategoryEditRequest;
use Illuminate\Support\Str;


class AdminCategory extends Controller
{

    public function index()
    {
        return view('admin/category', [
            'languages' => config('app.languages'),
            "categories" => CategoryModel::where('parent_id', 0)->orderBy('sort', 'asc')->orderBy('title', 'asc')->get(),
        ]);
    }

    public function create()
    {
        //
    }

    public function store(CategoryAddRequest $request)
    {
        $request['slug'] = Str::slug($request['slug'], '-');
        CategoryModel::create( $request->all() );
        if(isset($request['parent_id'])){
            return redirect()->route('admin.category.show', $request['parent_id'])->with( 'addSuccessful', 'CategoryAddSuccessful' );
        }
        return redirect()->route('admin.category.index')->with( 'addSuccessful', 'CategoryAddSuccessful' );
    }

    public function show(Request $request, $id)
    {
        $message = $request->session()->get('addSuccessful');
        if($id==0){
            if($message!=null){
                return redirect()->route('admin.category.index')->with( 'addSuccessful', $message );
            }
            return redirect()->route('admin.category.index');
        }
        return view('admin/category', [
            'languages' => config('app.languages'),
            "categories" => CategoryModel::where('parent_id', $id)->orderBy('sort', 'asc')->orderBy('title', 'asc')->get(),
            "category" => CategoryModel::find($id),
        ]);
    }

    public function edit($id)
    {

        $category = CategoryModel::find($id);
        $langs = LangModel::where('status',1)->orderBy('sort', 'asc')->get();
        $translateTitle = TranslateModel::traslateStr($category->title, $langs);
        return view('admin/category_edit', [
            'languages' => config('app.languages'),
            'category' => $category,
            'titles' => CategoryModel::getTitles($id),
            'translateTitle' => $translateTitle,
            "langs" => $langs,
        ]);
    }

    public function update(CategoryEditRequest $request, $id)
    {
        CategoryModel::updateData($id, $request->except(['_token', '_method']), $request->file('image'));
        CategoryTitlesModel::updateData($id, $request->except(['_token', '_method', 'title', 'slug', 'status']));
        $category = CategoryModel::find($id);
        return redirect()->route('admin.category.show', $category->parent_id)->with( 'addSuccessful', 'LangUpdateSuccessful' );
    }

    public function destroy(Request $request, $id)
    {
        $message = "CategoryDeleteNotEmpty";
        $child = CategoryModel::where('parent_id', $request['id'])->first();
        $category = CategoryModel::find($request['id']);
        if($child==null){
            CategoryModel::destroy($request['id']);
            CategoryExpressionModel::where('category_id', $request['id'])->delete();
            CategoryTitlesModel::where('category_id', $request['id'])->delete();
            $message = "CategoryDeleteSuccessful";
        }
        return redirect()->route('admin.category.show', $category->parent_id)->with( 'addSuccessful', $message );
    }
}
