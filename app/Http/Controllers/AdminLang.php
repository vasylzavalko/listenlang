<?php

namespace App\Http\Controllers;

use App\Http\Requests\LangAddRequest;
use App\Http\Requests\LangEditRequest;
use App\Models\AdminLang as LangModel;
use Illuminate\Http\Request;

class AdminLang extends Controller
{

    public function index()
    {
        return view('admin/langs', [
            'languages' => config('app.languages'),
            "langs" => LangModel::orderBy('sort', 'asc')->get(),
        ]);
    }

    public function store(LangAddRequest $request)
    {
        LangModel::create( $request->all() );
        return redirect()->route('admin.lang.index')->with( 'addSuccessful', 'LangAddSuccessful' );
    }

    public function edit($id)
    {
        return view('admin/langs_edit', [
            'languages' => config('app.languages'),
            'lang' => LangModel::find($id),
        ]);
    }

    public function update(LangEditRequest $request, $id)
    {
        LangModel::updateData($id, $request->except(['_token', '_method']));
        return redirect()->route('admin.lang.index')->with( 'addSuccessful', 'LangUpdateSuccessful' );
    }

    public function destroy(Request $request, $id)
    {
        LangModel::updateDataDestroy($request['id']);
        LangModel::destroy($request['id']);
        return redirect()->route('admin.lang.index')->with( 'addSuccessful', 'LangDeleteSuccessful' );
    }

}
