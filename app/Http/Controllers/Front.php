<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Front as FrontModel;

class Front extends Controller
{
    public function index()
    {
        return view('index', [
            'languages' => config('app.languages'),
            'user' =>  Auth::user(),
            'categories' => FrontModel::categoryTree(),
            'langs' => FrontModel::langs(),
            'from' => session('from'),
            'to' => session('to'),
        ]);
    }

    public function expression(Request $request)
    {
        if($request->query('c') && is_numeric($request->query('c'))){
            $category = FrontModel::getCategory($request->query('c'));
            $expression = FrontModel::expressions($request->query('c'));
            if(count($category)>0 && count($expression)>0){
                FrontModel::saveStatistic($request->query('c'));
                return view('expression', [
                    'languages' => config('app.languages'),
                    'user' =>  Auth::user(),
                    'category' => $category,
                    'expressions' => $expression,
                    'langs' => FrontModel::langs(),
                    'from' => session('from'),
                    'to' => session('to'),
                ]);
            }
        }
        return redirect()->route('home')->with( 'message', 'CategoryClearSelectOther' );;
    }

    public function expressionJson(Request $request)
    {
        if($request->query('c') && is_numeric($request->query('c'))) {
            $expressions = FrontModel::expressions($request->query('c'));
            return response()->json($expressions);
        }
        return response()->json([]);
    }

    public function about()
    {
        return view('about', [
            'languages' => config('app.languages'),
            'user' =>  Auth::user(),
        ]);
    }

}
