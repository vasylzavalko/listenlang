<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class IsLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $locales = config('app.locales');
        $firstSegment = request()->segment(1);

        if ( !$request->session()->has('locale') ) {

            session(['locale' => config('app.fallback_locale')]);
            App::setLocale( config('app.fallback_locale') );

        } else {

            if (in_array($firstSegment, $locales) && $firstSegment != config('fortify.prefix')) {
                session(['locale' => $firstSegment]);
                App::setLocale($firstSegment);
            } elseif ($firstSegment == config('fortify.prefix')) {
                App::setLocale(session('locale'));
            } else {
                session(['locale' => config('app.fallback_locale')]);
                App::setLocale(config('app.fallback_locale'));
            }

        }

        return $next($request);
    }
}
