<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\AdminLang as LangModel;
use Illuminate\Support\Facades\App;

class IsTranslate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $lang = LangModel::get();
        if ( !$request->session()->has('from') ) {
            session(['from' => $lang[0]['id']]);
        }
        if ( !$request->session()->has('to') ) {
            session(['to' => $lang[1]['id']]);
        }
        return $next($request);
    }
}
