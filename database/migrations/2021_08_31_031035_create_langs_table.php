<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('langs', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('status')->default(0);
            $table->smallInteger('sort')->default(0);
            $table->string('title', 80);
            $table->string('code', 20);
            $table->string('voice_code', 10);
            $table->string('voice_name', 30);
            $table->timestamps();
            $table->softDeletes();
        });
        DB::table('langs')->insert(
            array(
                'status' => 1,
                'sort' => 1,
                'title' => "English",
                'code' => "en",
                'voice_code' => "en-US",
                'voice_name' => "en-US-Wavenet-J",
            )
        );
        DB::table('langs')->insert(
            array(
                'status' => 1,
                'sort' => 2,
                'title' => "Українська",
                'code' => "uk",
                'voice_code' => "uk-UA",
                'voice_name' => "uk-UA-Wavenet-A",
            )
        );
        DB::table('langs')->insert(
            array(
                'status' => 1,
                'sort' => 3,
                'title' => "Русский",
                'code' => "ru",
                'voice_code' => "ru-RU",
                'voice_name' => "ru-RU-Wavenet-D",
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('langs');
    }
}
