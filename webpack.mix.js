const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/sw.js', 'public')
    .sass('resources/sass/app.scss', 'public/css', []).options({ processCssUrls: false });


