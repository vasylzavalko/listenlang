<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$firstSegment = request()->segment(1);
$locales = config('app.locales');

if ( in_array( $firstSegment, $locales ) && $firstSegment != config('fortify.prefix') ) {

    // Якщо перший сегмен локаль UA, RU і не сторінки авторизації/реєстрації
    // То запускаємо роути в групі локалі
    Route::group(['prefix' => $firstSegment], function()
    {
        include "routes.php";
    });

} else {

    // Локаль по замовчуванню EN або сторінки авторизації/реєстрації
    include "routes.php";

}

Route::get('/clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    return "Кэш очищен.";
});
