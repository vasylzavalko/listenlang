<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front;
use App\Http\Controllers\Admin;
use App\Http\Controllers\AdminLang;
use App\Http\Controllers\AdminCategory;
use App\Http\Controllers\AdminExpression;
use App\Http\Controllers\AdminUser;
use App\Http\Controllers\AdminTag;
use App\Http\Controllers\AdminAction;

// Роути фронтальної частини
Route::get('/', [Front::class, 'index'])->name('home');
Route::get('/about', [Front::class, 'about'])->name('about');
Route::get('/expression', [Front::class, 'expression'])->name('expression');
Route::post('/action/selectLang', [AdminAction::class, 'selectLang']);
Route::get('/expressionJson', [Front::class, 'expressionJson']);

// Роути адмінки
Route::middleware(['auth','is.admin'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/', function(){
            return redirect()->route('admin.expression.index');
        })->name('admin');
        Route::resource('/lang', AdminLang::class, [ 'as' => 'admin' ]);
        Route::resource('/category', AdminCategory::class, [ 'as' => 'admin' ]);
        Route::resource('/expression', AdminExpression::class, [ 'as' => 'admin' ]);
        Route::resource('/user', AdminUser::class, [ 'as' => 'admin' ]);
        Route::get('/tag', [AdminTag::class, 'index'])->name('tag');
        Route::post('/user/group', [AdminUser::class, 'changeGroup'])->name('changeGroup');
        Route::post('/action/sorting', [AdminAction::class, 'sorting']);
        Route::post('/action/tag', [AdminAction::class, 'tag']);
        Route::post('/action/deleteCategoryImage', [AdminAction::class, 'deleteCategoryImage']);
    });
});

Route::get('auth/redirect', function(){
    if ( session('locale')==config('app.fallback_locale') ){
        return redirect('/');
    } else {
        return redirect('/'.session('locale'));
    }
});
