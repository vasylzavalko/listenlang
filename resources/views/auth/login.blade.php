@extends('layouts.home')

@section('content')
    <div data-uk-height-viewport="expand: true" class="uk-section uk-section-small uk-section-muted uk-flex uk-flex-center uk-flex-middle">
        <div class="auth-card">
            <div class="uk-card uk-card-default uk-card-body">
                <h2 class="uk-card-title">{{ __('home.loginTitle') }}</h2>
                <form method="POST" action="{{ route('login') }}" class="uk-form-stacked">
                    @csrf
                    <div class="uk-margin">
                        <label for="email" class="uk-form-label">
                            {{ __('home.emailAddress') }}
                        </label>
                        <div class="uk-form-control">
                            <input class="uk-input @error('email') uk-form-danger @enderror" name="email" id="email"
                                   type="email"
                                   value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <span class="uk-text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label for="password" class="uk-form-label">
                            {{ __('home.password') }}
                        </label>
                        <div class="uk-form-control">
                            <input id="password" type="password"
                                   class="uk-input @error('password') uk-form-danger @enderror" name="password" required
                                   autocomplete="current-password">
                            @error('password')
                            <span class="uk-text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="uk-margin">
                        <div class="uk-form-control">
                            <input class="uk-checkbox" type="checkbox" name="remember"
                                   id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">
                                {{ __('home.rememberMe') }}
                            </label>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <div class="uk-grid-small" data-uk-grid>
                            <div class="uk-width-auto@s">
                                <div class="uk-form-control">
                                    <button type="submit" class="uk-button uk-button-primary">
                                        {{ __('home.loginButton') }}
                                    </button>
                                </div>
                            </div>
                            <div class="uk-width-expand@s uk-flex uk-flex-middle">
                                @if (Route::has('password.request'))
                                    <a href="{{ route('password.request') }}">
                                        {{ __('home.forgotYourPassword') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
