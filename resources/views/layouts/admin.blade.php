<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>
<body>
<div id="app">
    <header data-uk-sticky>
        <div data-uk-grid class="uk-grid-small">
            <div class="uk-width-auto">
                <a href="@if ( session('locale')!=config('app.fallback_locale') )/{{ session('locale') }}@endif/admin" class="logo">{{ __('home.listenlang') }}</a>
            </div>
            <div class="uk-width-expand">
                <ul class="uk-visible@m">
                    <li><a href="{{ route('admin.expression.index') }}">{{ __('admin.expressions') }}</a></li>
                    <li><a href="{{ route('admin.category.index') }}">{{ __('admin.categories') }}</a></li>
                    <li><a href="{{ route('tag') }}">{{ __('admin.tags') }}</a></li>
                    <li><a href="{{ route('admin.lang.index') }}">{{ __('admin.langs') }}</a></li>
                    <li><a href="{{ route('admin.user.index') }}">{{ __('admin.users') }}</a></li>
                </ul>
            </div>
            <div class="uk-width-auto uk-visible@m">
                <ul>
                    <li><a href="{{ route('home')  }}">{{ __('admin.backToSite') }}</a></li>
                </ul>
            </div>
            <div class="uk-width-auto uk-flex uk-flex-middle">
                <ul class="lang uk-visible@m">
                    @foreach ($languages as $lang)
                        <li><a href="@if ( $lang!=config('app.fallback_locale') )/{{ $lang }}@endif/admin" class="@if ( $lang==session('locale') )active @endif">{{ __('home.'.$lang) }}</a></li>
                    @endforeach
                </ul>
                <a href="#mobile-menu" data-uk-toggle class="uk-hidden@m"><span data-uk-icon="icon:menu;ratio:1.2"></span></a>
            </div>
        </div>
    </header>
    <div id="mobile-menu" data-uk-offcanvas="mode: push; overlay: true">
        <div class="uk-offcanvas-bar">
            <button class="uk-offcanvas-close" type="button" data-uk-close></button>
            <div class="container">
                <ul>
                    <li><a href="{{ route('admin') }}">{{ __('admin.expressions') }}</a></li>
                    <li><a href="{{ route('admin') }}">{{ __('admin.categories') }}</a></li>
                    <li><a href="{{ route('admin') }}">{{ __('admin.tags') }}</a></li>
                    <li><a href="{{ route('admin') }}">{{ __('admin.langs') }}</a></li>
                    <li><a href="{{ route('admin') }}">{{ __('admin.users') }}</a></li>
                    <li><a href="{{ route('home')  }}">{{ __('admin.backToSite') }}</a></li>
                </ul>
            </div>
            <div class="container-bottom">
                <ul class="lang">
                    @foreach ($languages as $lang)
                        <li><a href="@if ( $lang!=config('app.fallback_locale') )/{{ $lang }}@endif/admin" class="@if ( $lang==session('locale') )active @endif">{{ __('home.'.$lang) }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <main class="admin" data-uk-height-viewport="expand: true">
        @yield('content')
    </main>

    <footer>
        <div data-uk-grid class="uk-grid-collapse">
            <div class="uk-width-auto@s uk-text-center">2021 ListenLang</div>
            <div class="uk-width-expand uk-visible@s"></div>
            <div class="uk-width-auto@s uk-text-center">Developed by <a href="https://vasylzavalko.com/" target="_blank">Vasyl Zavalko</a></div>
        </div>
    </footer>
</div>
</body>
</html>
