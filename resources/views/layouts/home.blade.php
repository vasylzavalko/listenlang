<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="apple-mobile-web-app-status-bar" content="#1053ba">
    <meta name="theme-color" content="#1053ba">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link rel="manifest" href="/manifest.json">
    <link rel="shortcut icon" href="/images/template/listenlang-icon-32.png" sizes="32x32" type="image/png">
    <link rel="apple-touch-icon" href="/images/template/listenlang-icon-logo-96.png">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
<div id="app">
    <header data-uk-sticky>
        <div data-uk-grid class="uk-grid-small">
            <div class="uk-width-auto">
                <a href="/@if ( session('locale')!=config('app.fallback_locale') ){{ session('locale') }}@endif" class="logo">{{ __('home.listenlang') }}</a>
            </div>
            <div class="uk-width-expand">

            </div>
            <div class="uk-width-auto uk-flex uk-flex-middle uk-visible@s">
                <ul>
                    @guest
                        @if (Route::has('login'))
                            <li>
                                <a href="{{ route('login') }}">{{ __('home.login') }}</a>
                            </li>
                        @endif
                        @if (Route::has('register'))
                            <li>
                                <a href="{{ route('register') }}">{{ __('home.register') }}</a>
                            </li>
                        @endif
                    @else
                        <li>
                            <div class="uk-inline">
                                <a href="#" class="icon"><span data-uk-icon="icon:user;ratio:.8" class="icon"></span>{{ Auth::user()->name }}</a>
                                <div data-uk-dropdown="mode: click">
                                    <ul>
                                        @if ( $user->group_id==1 )
                                            <li><a href="{{ route('admin') }}">{{ __('home.adminpanel') }}</a></li>
                                        @endif
                                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('home.logout') }}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest
                </ul>
            </div>
            <div class="uk-width-auto uk-flex uk-flex-middle">
                <ul class="lang uk-visible@s">
                    @foreach ($languages as $lang)
                        <li><a href="/@if ( $lang!=config('app.fallback_locale') ){{ $lang }}@endif" class="@if ( $lang==session('locale') )active @endif">{{ __('home.'.$lang) }}</a></li>
                    @endforeach
                </ul>
                <a href="#mobile-menu" data-uk-toggle class="uk-hidden@s"><span data-uk-icon="icon:menu;ratio:1.2"></span></a>
            </div>
        </div>
    </header>
    <div id="mobile-menu" data-uk-offcanvas="mode: push; overlay: true">
        <div class="uk-offcanvas-bar">
            <button class="uk-offcanvas-close" type="button" data-uk-close></button>
            <div class="container">
                <ul>
                    @guest
                        @if (Route::has('login'))
                            <li><a class="small" href="{{ route('login') }}">{{ __('home.login') }}</a></li>
                        @endif
                        @if (Route::has('register'))
                            <li><a class="small" href="{{ route('register') }}">{{ __('home.register') }}</a></li>
                        @endif
                    @else
                        <li><p class="icon name"><span data-uk-icon="icon:user;ratio:.8" class="icon"></span>{{ Auth::user()->name }}</p></li>
                        <li><a class="logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('home.logout') }}</a></li>
                    @endguest
                        <li class="divider"></li>
                        <li><b><a href="{{ route('home') }}">{{ __('home.startStudy') }}</a></b></li>
                        <li><b><a href="{{ route('about') }}">{{ __('home.aboutApp') }}</a></b></li>
                </ul>
            </div>
            <div class="container-bottom">
                <ul class="lang">
                    @foreach ($languages as $lang)
                        <li><a href="/@if ( $lang!=config('app.fallback_locale') ){{ $lang }}@endif" class="@if ( $lang==session('locale') )active @endif">{{ __('home.'.$lang) }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <main data-uk-height-viewport="expand: true">
        @yield('content')
    </main>

    <footer class="uk-visible@s">
        <div data-uk-grid class="uk-grid-collapse">
            <div class="uk-width-auto@s uk-text-center">2021 ListenLang</div>
            <div class="uk-width-expand uk-visible@s">
                <ul>
                    <li><a href="{{ route('home') }}">{{ __('home.startStudy') }}</a></li>
                    <li><a href="{{ route('about') }}">{{ __('home.aboutApp') }}</a></li>
                </ul>
            </div>
            <div class="uk-width-auto@s uk-text-center">Developed by <a href="https://vasylzavalko.com/" target="_blank">Vasyl Zavalko</a></div>
        </div>
    </footer>
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
