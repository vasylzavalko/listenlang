@extends('layouts.admin')

@section('content')

    <div data-uk-grid class="uk-grid-small">
        <div class="uk-width-1-1">
            <a href="{{ route('admin.category.show', $category->parent_id) }}"><span uk-icon="icon: arrow-left" class="uk-margin-small-right"></span>{{ __('admin.backToCategory') }}</a>
        </div>
        <div class="uk-width-1-1">
            <h1>{{ __('admin.editTitle') }} / <span class="uk-text-danger uk-text-bold">{{ $category->title }}</span></h1>
        </div>

        <div class="uk-width-1-1">
            <form method="POST" action="{{ route('admin.category.update', ["category" => $category->id]) }}" class="uk-grid-small" data-uk-grid enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="uk-width-1-1">
                    <label>{{ __('admin.categoryTitle') }}</label>
                    <input type="text" class="uk-input uk-width-1-1" value="{{ old('title') ?? $category->title }}" name="title" required maxlength="60" autocomplete="off">
                    @error('title')
                    <span class="validation-error">
                            {!! $message !!}
                        </span>
                    @enderror
                </div>
                <div class="uk-width-1-1">
                    <label>{{ __('admin.categorySlug') }}</label>
                    <input type="text" class="uk-input uk-width-1-1" value="{{ old('slug') ?? $category->slug }}" name="slug" required maxlength="60" autocomplete="off">
                    @error('slug')
                    <span class="validation-error">
                            {!! $message !!}
                        </span>
                    @enderror
                </div>
                <div class="uk-width-1-1">
                    <label>{{ __('admin.status') }}</label>
                    <select name="status" class="uk-select">
                        <option value="0" @if($category->status==0)selected @endif>Close</option>
                        <option value="1" @if($category->status==1)selected @endif>Open</option>
                    </select>
                </div>
                <div class="uk-width-1-1">
                    <div class="box-border">
                        <div class="box-border-label">{{ __('admin.translate') }}</div>
                        <div data-uk-grid class="uk-grid-small">
                            @foreach($langs as $value)
                                <div class="uk-width-1-1">
                                    <label>{{ $value->title }}</label>
                                    <input type="text" class="uk-input uk-width-1-1" value="@if(isset($titles[$value->id])){{ $titles[$value->id]->title }}@else{{ $translateTitle[$value->id] }}@endif" name="titles_{{ $value->id }}" autocomplete="off" required>
                                    <span class="uk-text-warning uk-text-italic">Google Translate: {{ $translateTitle[$value->id] }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @if($category->image!="none")
                    <div class="uk-width-small image-hide">
                        <img src="/files/category/{{ $category->image }}">
                    </div>
                @endif
                <div class="uk-width-expand">
                    <label>{{ __('admin.image') }}</label>
                    <div class="uk-width-1-1" data-uk-form-custom="target: true">
                        <input type="file" name="image">
                        <input class="uk-input uk-width-1-1" type="text" placeholder="{{ __('admin.imageSelect') }}" disabled>
                    </div>
                    @if($category->image!="none")
                    <p><a href="#" class="delete-image image-hide" data-uk-toggle="target: .image-hide" data-id="{{ $category->id }}">{{ __('admin.imageDelete') }}</a></p>
                    @endif
                </div>
                <div class="uk-width-1-1 uk-margin-medium-top">
                    <button type="submit" class="uk-button uk-button-primary">{{ __('admin.saveButton') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
