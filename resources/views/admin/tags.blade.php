@extends('layouts.admin')

@section('content')

    <div data-uk-grid class="uk-grid-small">
        <div class="uk-width-1-1">
            <h1>{{ __('admin.tagsTitle') }}</h1>
        </div>
        <div class="uk-width-1-1">
            <div data-uk-grid class="uk-grid-small">
            @foreach($langs as $lang)
                <div class="uk-width-auto">
                    <a href="{{ route('tag') }}?c={{ $lang->id }}" class="uk-button uk-button-small @if($langActive==$lang->id) uk-button-primary @else uk-button-default @endif ">{{ $lang->title }}</a>
                </div>
            @endforeach
            </div>
        </div>
        @if($tags->isEmpty())
            <div class="uk-width-1-1">
                <div class="alert uk-text-bold">
                    {{ __('admin.noTags') }}
                </div>
            </div>
        @endif
        <div class="uk-width-1-1"><hr></div>
        <div class="uk-width-1-1">
            <div class="uk-grid-small" data-uk-grid>
            @foreach ($tags as $tag)
                <div class="uk-width-1-6">
                    <p>{{ $tag->title }}</p>
                </div>
            @endforeach
            </div>
        </div>
        <div class="uk-width-1-1">
            {{ $tags->links('pagination') }}
        </div>
    </div>
@endsection
