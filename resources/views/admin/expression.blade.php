@extends('layouts.admin')

@section('content')

    <div data-uk-grid class="uk-grid-small">
        <div class="uk-width-expand">
            <h1>
                {{ __('admin.expressionsTitle') }}
                @if($categoryActive)
                    <span class="category"> / {{ $categoryActive->title }}</span>
                @endif
            </h1>
        </div>
        <div class="uk-width-auto">
            <a href="#add" data-uk-toggle class="uk-button uk-button-primary">{{ __('admin.addExpressionButton') }}</a>
        </div>
        @if (session('addSuccessful'))
            <div class="uk-width-1-1">
                <div class="uk-alert-success uk-text-bold" data-uk-alert>
                    <a class="uk-alert-close" data-uk-close></a>
                    {{ __('admin.alertMessage'.session('addSuccessful')) }}
                </div>
            </div>
        @endif

        <div class="uk-width-1-1" id="add" @if(!$errors->any())hidden @endif>
            <div class="alert">
                <form method="POST" action="{{ route('admin.expression.store') }}" class="uk-grid-small" data-uk-grid>
                    @csrf
                    <div class="uk-width-1-1">
                        <h2>{{ __('admin.expressionAddTitle') }}</h2>
                    </div>
                    <div class="uk-width-1-1">
                        <label>{{ __('admin.expressionTitle') }}</label>
                        <input type="text" class="uk-input uk-width-1-1" value="{{ old('title') }}" name="title" required maxlength="255" autocomplete="off">
                        @error('title')
                        <span class="validation-error">
                            {!! $message !!}
                        </span>
                        @enderror
                    </div>
                    <div class="uk-width-expand">
                        <button type="submit" class="uk-button uk-button-primary">{{ __('admin.addButton') }}</button>
                    </div>
                </form>
            </div>
        </div>
        @if($expressions->isEmpty())
            <div class="uk-width-1-1">
                <div class="alert uk-text-bold">
                    {{ __('admin.noExpression') }}
                </div>
            </div>
        @endif
        <div class="uk-width-1-1">
            <div data-uk-grid class="uk-grid-small">
                <div class="uk-width-expand">
                    <div class="expression">
                        <div data-uk-grid class="uk-grid-collapse items">
                            @foreach ($expressions as $expression)
                                <div class="uk-width-1-1">
                                    <div class="item">
                                        <div data-uk-grid class="uk-grid-small">
                                            <div class="uk-width-auto uk-flex uk-flex-middle"><span class="status status-{{ $expression->status }}"></span></div>
                                            <div class="uk-width-auto uk-flex uk-flex-middle"><span class="item-id">ID: {{ $expression->id }}</span></div>
                                            <div class="uk-width-expand"><b>{{ $expression->title }}</b></div>
                                            <div class="uk-width-auto">
                                                <a href="{{ route('admin.expression.edit', $expression->id) }}"><span data-uk-icon="icon: file-edit" class=""></span></a>
                                            </div>
                                            <div class="uk-width-auto">
                                                <a href="#delete" class="delete-link" data-id="{{ $expression->id }}" data-title="ID: {{ $expression->id }} / Title: {{ $expression->title }}" data-uk-toggle><span data-uk-icon="icon: trash" class=""></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="uk-width-1-1 uk-margin-small-top">
                                {{ $expressions->withQueryString()->links('pagination') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium">
                    <div class="box-border">
                        <div class="box-border-label">{{ __('admin.categories') }}</div>
                        <ul class="category">
                            <li><a class="@if(!isset($categoryActive->id)) active @endif" href="{{ route('admin.expression.index') }}">{{ __('admin.allCategory') }}</a></li>
                            @foreach($categoryTree as $value)
                                <li><a class="@if(isset($categoryActive->id) && $value['id']==$categoryActive->id) active @endif" href="{{ route('admin.expression.index') }}?c={{ $value['id'] }}">{{ $value['title'] }}</a></li>
                                @if(isset($value['childs']) && count($value['childs'])>0)
                                    @foreach($value['childs'] as $valueChild)
                                        <li class="second"><a class="@if(isset($categoryActive->id) && $valueChild['id']==$categoryActive->id) active @endif" href="{{ route('admin.expression.index') }}?c={{ $valueChild['id'] }}">{{ $valueChild['title'] }}</a></li>
                                    @endforeach
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="delete" class="uk-flex-top" data-uk-modal>
        <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
            <button class="uk-modal-close-default" type="button" data-uk-close></button>
            <div data-uk-grid class="uk-grid-small">
                <div class="uk-width-1-1"><h3 class="uk-text-danger uk-text-bold">{{ __('admin.attention') }}</h3></div>
                <div class="uk-width-1-1">{{ __('admin.modalDeleteDesc') }}</div>
                <div class="uk-width-1-1 uk-text-bold"><span class="delete-title"></span></div>
                <div class="uk-width-expand uk-flex uk-flex-right">
                    <button class="uk-button uk-button-default uk-modal-close" type="button">{{ __('admin.noButton') }}</button>
                </div>
                <div class="uk-width-auto">
                    <form method="POST" action="{{ route('admin.expression.destroy', 0) }}" class="delete-form">
                        @csrf
                        @method('DELETE')
                        <input type="text" value="1" name="id" class="delete-id" required hidden>
                        <button type="submit" class="uk-button uk-button-primary">{{ __('admin.yesButton') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
