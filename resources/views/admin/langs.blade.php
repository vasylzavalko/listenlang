@extends('layouts.admin')

@section('content')

    <div data-uk-grid class="uk-grid-small">
        <div class="uk-width-expand">
            <h1>{{ __('admin.langsTitle') }}</h1>
        </div>
        <div class="uk-width-auto">
            <a href="#add" data-uk-toggle class="uk-button uk-button-primary">{{ __('admin.addLangsButton') }}</a>
        </div>
        @if (session('addSuccessful'))
            <div class="uk-width-1-1">
                <div class="uk-alert-success uk-text-bold" data-uk-alert>
                    <a class="uk-alert-close" data-uk-close></a>
                    {{ __('admin.alertMessage'.session('addSuccessful')) }}
                </div>
            </div>
        @endif

        <div class="uk-width-1-1" id="add" @if(!$errors->any())hidden @endif>
            <div class="alert">
                <form method="POST" action="{{ route('admin.lang.store') }}" class="uk-grid-small" data-uk-grid>
                    @csrf
                    <div class="uk-width-1-1">
                        <h2>{{ __('admin.langsAddTitle') }}</h2>
                    </div>
                    <div class="uk-width-1-1">
                        <label>{{ __('admin.langTitle') }}</label>
                        <input type="text" class="uk-input uk-width-1-1" value="{{ old('title') }}" name="title" required maxlength="60" autocomplete="off">
                        @error('title')
                        <span class="validation-error">
                            {!! $message !!}
                        </span>
                        @enderror
                    </div>
                    <div class="uk-width-1-1">
                        <label>{{ __('admin.langCode') }}</label>
                        <input type="text" class="uk-input uk-width-1-1" value="{{ old('code') }}" name="code" required maxlength="6" autocomplete="off">
                        @error('code')
                        <span class="validation-error">
                            {!! $message !!}
                        </span>
                        @enderror
                    </div>
                    <div class="uk-width-expand">
                        <button type="submit" class="uk-button uk-button-primary">{{ __('admin.addButton') }}</button>
                    </div>
                    <div class="uk-width-auto uk-flex uk-flex-middle">
                        <a href="https://cloud.google.com/translate/docs/languages" target="_blank">{{ __('admin.langLocales') }}</a>
                    </div>
                </form>
            </div>
        </div>
        @if($langs->isEmpty())
            <div class="uk-width-1-1">
                <div class="alert uk-text-bold">
                    {{ __('admin.noEntries') }}
                </div>
            </div>
        @endif
        <div class="uk-width-1-1">
            <div data-uk-grid data-uk-sortable="handle: .sortable-handle" class="uk-grid-collapse items" id="sorting" data-table="langs">
                @foreach ($langs as $lang)
                    <div class="uk-width-1-1" id="{{ $lang->id }}">
                        <div class="item">
                            <div data-uk-grid class="uk-grid-small">
                                <div class="uk-width-auto"><span data-uk-icon="icon: move" class="sortable-handle uk-drag handle"></span></div>
                                <div class="uk-width-auto uk-flex uk-flex-middle"><span class="status status-{{ $lang->status }}"></span></div>
                                <div class="uk-width-auto uk-flex uk-flex-middle"><span class="item-id">ID: {{ $lang->id }}</span></div>
                                <div class="uk-width-expand"><b>{{ $lang->title }}</b> / {{ $lang->code }}</div>
                                <div class="uk-width-auto">
                                    <a href="{{ route('admin.lang.edit', $lang->id) }}"><span data-uk-icon="icon: file-edit" class=""></span></a>
                                </div>
                                <div class="uk-width-auto">
                                    <a href="#delete" class="delete-link" data-id="{{ $lang->id }}" data-title="ID: {{ $lang->id }} / Title: {{ $lang->title }}" data-uk-toggle><span data-uk-icon="icon: trash" class=""></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div id="delete" class="uk-flex-top" data-uk-modal>
        <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div data-uk-grid class="uk-grid-small">
                <div class="uk-width-1-1"><h3 class="uk-text-danger uk-text-bold">{{ __('admin.attention') }}</h3></div>
                <div class="uk-width-1-1">{{ __('admin.modalDeleteDesc') }}</div>
                <div class="uk-width-1-1 uk-text-bold"><span class="delete-title"></span></div>
                <div class="uk-width-expand uk-flex uk-flex-right">
                    <button class="uk-button uk-button-default uk-modal-close" type="button">{{ __('admin.noButton') }}</button>
                </div>
                <div class="uk-width-auto">
                    <form method="POST" action="{{ route('admin.lang.destroy', 0) }}" class="delete-form">
                        @csrf
                        @method('DELETE')
                        <input type="text" value="1" name="id" class="delete-id" required hidden>
                        <button type="submit" class="uk-button uk-button-primary">{{ __('admin.yesButton') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
