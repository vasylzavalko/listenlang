@extends('layouts.admin')

@section('content')

    <div data-uk-grid class="uk-grid-small">
        <div class="uk-width-1-1">
            <a href="{{ route('admin.expression.index') }}"><span uk-icon="icon: arrow-left" class="uk-margin-small-right"></span>{{ __('admin.backToExpression') }}</a>
        </div>
        <div class="uk-width-1-1">
            <h1>{{ __('admin.editTitle') }} / <span class="uk-text-danger uk-text-bold">{{ $expression->title }}</span></h1>
        </div>
        @if (session('addSuccessful'))
            <div class="uk-width-1-1">
                <div class="uk-alert-success uk-text-bold" data-uk-alert>
                    <a class="uk-alert-close" data-uk-close></a>
                    {{ __('admin.alertMessage'.session('addSuccessful')) }}
                </div>
            </div>
        @endif

        <div class="uk-width-1-1">
            <form method="POST" action="{{ route('admin.expression.update', ["expression" => $expression->id]) }}" class="uk-grid-small" data-uk-grid>
                @csrf
                @method('PUT')
                <div class="uk-width-1-1">
                    <label>{{ __('admin.expressionTitle') }}</label>
                    <input type="text" class="uk-input uk-width-1-1" value="{{ old('title') ?? $expression->title }}" name="title" required maxlength="255" autocomplete="off">
                    @error('title')
                    <span class="validation-error">
                            {!! $message !!}
                        </span>
                    @enderror
                </div>
                <div class="uk-width-1-1">
                    <label>{{ __('admin.status') }}</label>
                    <select name="status" class="uk-select">
                        <option value="0" @if($expression->status==0)selected @endif>Close</option>
                        <option value="1" @if($expression->status==1)selected @endif>Open</option>
                    </select>
                </div>
                <div class="uk-width-1-1">
                    <div class="box-border">
                        <div class="box-border-label">{{ __('admin.translate') }}</div>
                        <div data-uk-grid class="uk-grid-small">
                            @foreach($langs as $value)
                                <div class="uk-width-1-1">
                                    <div data-uk-grid class="uk-grid-small">
                                        <div class="uk-width-expand">
                                            <label>{{ $value->title }}</label>
                                            <input type="text" class="uk-input uk-width-1-1" value="@if(isset($titles[$value->id])){{ $titles[$value->id]->title }}@else{{ $translateTitle[$value->id] }}@endif" name="titles_{{ $value->id }}" autocomplete="off" required>
                                            <span class="uk-text-warning uk-text-italic">Google Translate: {{ $translateTitle[$value->id] }}</span>
                                        </div>
                                        <div class="uk-width-medium@m uk-flex uk-flex-middle">
                                            @if(isset($titles[$value->id]))
                                                <audio controls src="/files/voice/{{ $expression->id }}/{{ $titles[$value->id]->voice }}">
                                                    Your browser does not support the audio element.
                                                </audio>
                                            @else
                                                no
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-1">
                    <div class="box-border">
                        <div class="box-border-label">{{ __('admin.categories') }}</div>
                        @if(count($category)>0)
                            <div class="uk-margin-small-bottom">
                                <a href="#" data-uk-toggle="target:.category-hidden">{{ __('admin.viewAllCategory') }}</a>
                            </div>
                        @endif
                        <ul class="category">
                            @foreach($categoryTree as $value)
                                <li class="@if(count($category)>0 && !in_array($value['id'], $category)) category-hidden @endif" @if(count($category)>0 && !in_array($value['id'], $category)) hidden @endif><input @if(in_array($value['id'], $category)) checked @endif class="uk-checkbox uk-margin-small-right" type="checkbox" name="category[]" value="{{ $value['id'] }}">{{ $value['title'] }}</li>
                                @if(isset($value['childs']) && count($value['childs'])>0)
                                    @foreach($value['childs'] as $valueChild)
                                        <li class="second @if(count($category)>0 && !in_array($valueChild['id'], $category)) category-hidden @endif" @if(count($category)>0 && !in_array($valueChild['id'], $category)) hidden @endif><input @if(in_array($valueChild['id'], $category)) checked @endif class="uk-checkbox uk-margin-small-right" type="checkbox" name="category[]" value="{{ $valueChild['id'] }}">{{ $valueChild['title'] }}</li>
                                    @endforeach
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="uk-width-1-1">
                    <div class="box-border">
                        <div class="box-border-label">{{ __('admin.tags') }}</div>
                        <div data-uk-grid class="uk-grid-small">
                            @foreach($langs as $lang)
                            <div class="uk-width-1-1">
                                <div data-uk-grid class="uk-grid-small">
                                    <div class="uk-width-small uk-flex uk-flex-middle uk-text-bold">{{ $lang->title }}:</div>
                                    <div class="uk-width-expand">
                                        <ul class="tags">
                                            @foreach($tagSelect[$lang->id] as $tag)
                                                <li><a href="#" class="tag-link @if(in_array($tag, $tags)) active @endif" data-expression="{{ $expression->id }}" data-lang="{{ $lang->id }}" data-tag="{{ $tag }}">{{ $tag }}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-1 uk-margin-medium-top">
                    <div data-uk-grid class="uk-grid-small">
                        <div class="uk-width-auto">
                            <button type="submit" name="submit" class="uk-button uk-button-primary" value="save">{{ __('admin.saveButton') }}</button>
                        </div>
                        <div class="uk-width-expand">
                            <button type="submit" name="submit" class="uk-button uk-button-primary" value="save-exit">{{ __('admin.saveExitButton') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
