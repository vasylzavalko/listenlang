@extends('layouts.admin')

@section('content')

    <div data-uk-grid class="uk-grid-small">
        <div class="uk-width-1-1">
            <a href="{{ route('admin.lang.index') }}"><span uk-icon="icon: arrow-left" class="uk-margin-small-right"></span>{{ __('admin.backToLangs') }}</a>
        </div>
        <div class="uk-width-1-1">
            <h1>{{ __('admin.editTitle') }} / <span class="uk-text-danger uk-text-bold">{{ $lang->title }}</span></h1>
        </div>

        <div class="uk-width-1-1">
            <form method="POST" action="{{ route('admin.lang.update', ["lang" => $lang->id]) }}" class="uk-grid-small" data-uk-grid>
                @csrf
                @method('PUT')
                <div class="uk-width-1-1">
                    <label>{{ __('admin.langTitle') }}</label>
                    <input type="text" class="uk-input uk-width-1-1" value="{{ old('title') ?? $lang->title }}" name="title" required maxlength="60" autocomplete="off">
                    @error('title')
                    <span class="validation-error">
                            {!! $message !!}
                        </span>
                    @enderror
                </div>
                <div class="uk-width-1-3@m">
                    <label>{{ __('admin.langCode') }}</label>
                    <input type="text" class="uk-input uk-width-1-1" value="{{ old('code') ?? $lang->code }}" name="code" required maxlength="6" autocomplete="off">
                    @error('code')
                    <span class="validation-error">
                            {!! $message !!}
                        </span>
                    @enderror
                </div>
                <div class="uk-width-1-3@m">
                    <label>{{ __('admin.langVoiceCode') }}</label>
                    <input type="text" class="uk-input uk-width-1-1" value="{{ old('voice_code') ?? $lang->voice_code }}" name="voice_code" required maxlength="6" autocomplete="off">
                </div>
                <div class="uk-width-1-3@m">
                    <label>{{ __('admin.langVoiceName') }}</label>
                    <input type="text" class="uk-input uk-width-1-1" value="{{ old('voice_name') ?? $lang->voice_name }}" name="voice_name" required maxlength="30" autocomplete="off">
                </div>
                <div class="uk-width-1-1">
                    <label>{{ __('admin.status') }}</label>
                    <select name="status" class="uk-select">
                        <option value="0" @if($lang->status==0)selected @endif>Close</option>
                        <option value="1" @if($lang->status==1)selected @endif>Open</option>
                    </select>
                </div>
                <div class="uk-width-expand">
                    <button type="submit" class="uk-button uk-button-primary">{{ __('admin.saveButton') }}</button>
                </div>
                <div class="uk-width-auto uk-flex uk-flex-middle">
                    <a href="https://cloud.google.com/translate/docs/languages" target="_blank">{{ __('admin.langLocales') }}</a>
                </div>
            </form>
        </div>
    </div>

@endsection
