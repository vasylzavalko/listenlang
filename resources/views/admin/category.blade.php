@extends('layouts.admin')

@section('content')

    <div data-uk-grid class="uk-grid-small">
        <div class="uk-width-expand">
            <h1>{{ __('admin.categoriesTitle') }}@if (isset($category)) / {{ $category->title }}@endif</h1>
        </div>
        <div class="uk-width-auto">
            <a href="#add" data-uk-toggle class="uk-button uk-button-primary">{{ __('admin.addCategoryButton') }}</a>
        </div>
        @if (isset($category))
        <div class="uk-width-1-1">
            <a href="{{ route('admin.category.show', $category->parent_id) }}"><span uk-icon="icon: arrow-left" class="uk-margin-small-right"></span>{{ __('admin.backToCategoryUp') }}</a>
        </div>
        @endif
        @if (session('addSuccessful'))
            <div class="uk-width-1-1">
                <div class="uk-alert-success uk-text-bold" data-uk-alert>
                    <a class="uk-alert-close" data-uk-close></a>
                    {{ __('admin.alertMessage'.session('addSuccessful')) }}
                </div>
            </div>
        @endif

        <div class="uk-width-1-1" id="add" @if(!$errors->any())hidden @endif>
            <div class="alert">
                <form method="POST" action="{{ route('admin.category.store') }}" class="uk-grid-small" data-uk-grid>
                    @csrf
                    @if (isset($category))
                        <input type="text" value="{{ $category->id }}" name="parent_id" required hidden>
                    @endif
                    <div class="uk-width-1-1">
                        <h2>{{ __('admin.categoryAddTitle') }}</h2>
                    </div>
                    <div class="uk-width-1-1">
                        <label>{{ __('admin.categoryTitle') }}</label>
                        <input type="text" class="uk-input uk-width-1-1" value="{{ old('title') }}" name="title" required maxlength="60" autocomplete="off">
                        @error('title')
                        <span class="validation-error">
                            {!! $message !!}
                        </span>
                        @enderror
                    </div>
                    <div class="uk-width-1-1">
                        <label>{{ __('admin.categorySlug') }}</label>
                        <input type="text" class="uk-input uk-width-1-1" value="{{ old('slug') }}" name="slug" required maxlength="60" autocomplete="off">
                        @error('slug')
                        <span class="validation-error">
                            {!! $message !!}
                        </span>
                        @enderror
                    </div>
                    <div class="uk-width-expand">
                        <button type="submit" class="uk-button uk-button-primary">{{ __('admin.addButton') }}</button>
                    </div>
                </form>
            </div>
        </div>
        @if($categories->isEmpty())
            <div class="uk-width-1-1">
                <div class="alert uk-text-bold">
                    {{ __('admin.noEntries') }}
                </div>
            </div>
        @endif
        <div class="uk-width-1-1">
            <div data-uk-grid data-uk-sortable="handle: .sortable-handle" class="uk-grid-collapse items" id="sorting" data-table="categories">
                @foreach ($categories as $category)
                    <div class="uk-width-1-1" id="{{ $category->id }}">
                        <div class="item">
                            <div data-uk-grid class="uk-grid-small">
                                <div class="uk-width-auto"><span data-uk-icon="icon: move" class="sortable-handle uk-drag handle"></span></div>
                                <div class="uk-width-auto uk-flex uk-flex-middle"><span class="status status-{{ $category->status }}"></span></div>
                                <div class="uk-width-auto uk-flex uk-flex-middle"><span class="item-id">ID: {{ $category->id }}</span></div>
                                <div class="uk-width-auto">
                                    <a href="{{ route('admin.category.show', $category->id) }}"><span data-uk-icon="icon: sign-in" class=""></span></a>
                                </div>
                                <div class="uk-width-expand"><b>{{ $category->title }}</b> <span class="uk-text-warning uk-text-italic uk-margin-small-left">slug: {{ $category->slug }}</span></div>
                                <div class="uk-width-auto">
                                    <a href="{{ route('admin.category.edit', $category->id) }}"><span data-uk-icon="icon: file-edit" class=""></span></a>
                                </div>
                                <div class="uk-width-auto">
                                    <a href="#delete" class="delete-link" data-id="{{ $category->id }}" data-title="ID: {{ $category->id }} / Title: {{ $category->title }}" data-uk-toggle><span data-uk-icon="icon: trash" class=""></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div id="delete" class="uk-flex-top" data-uk-modal>
        <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div data-uk-grid class="uk-grid-small">
                <div class="uk-width-1-1"><h3 class="uk-text-danger uk-text-bold">{{ __('admin.attention') }}</h3></div>
                <div class="uk-width-1-1">{{ __('admin.modalDeleteDesc') }}</div>
                <div class="uk-width-1-1 uk-text-bold"><span class="delete-title"></span></div>
                <div class="uk-width-expand uk-flex uk-flex-right">
                    <button class="uk-button uk-button-default uk-modal-close" type="button">{{ __('admin.noButton') }}</button>
                </div>
                <div class="uk-width-auto">
                    <form method="POST" action="{{ route('admin.category.destroy', 0) }}" class="delete-form">
                        @csrf
                        @method('DELETE')
                        <input type="text" value="1" name="id" class="delete-id" required hidden>
                        <button type="submit" class="uk-button uk-button-primary">{{ __('admin.yesButton') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
