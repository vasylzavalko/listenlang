@extends('layouts.admin')

@section('content')

    <div data-uk-grid class="uk-grid-small">
        <div class="uk-width-expand">
            <h1>{{ __('admin.usersTitle') }}</h1>
        </div>
        @if (session('addSuccessful'))
            <div class="uk-width-1-1">
                <div class="uk-alert-success uk-text-bold" data-uk-alert>
                    <a class="uk-alert-close" data-uk-close></a>
                    {{ __('admin.alertMessage'.session('addSuccessful')) }}
                </div>
            </div>
        @endif

        @if($users->isEmpty())
            <div class="uk-width-1-1">
                <div class="alert uk-text-bold">
                    {{ __('admin.noUsers') }}
                </div>
            </div>
        @endif
        <div class="uk-width-1-1">
            <div data-uk-grid data-uk-sortable="handle: .sortable-handle" class="uk-grid-collapse items" id="sorting" data-table="langs">
                @foreach ($users as $user)
                    <div class="uk-width-1-1" id="{{ $user->id }}">
                        <div class="item">
                            <div data-uk-grid class="uk-grid-small">
                                <div class="uk-width-auto uk-flex uk-flex-middle"><span class="item-id">ID: {{ $user->id }}</span></div>
                                <div class="uk-width-expand"><b><a href="#user-{{ $user->id }}" data-uk-toggle class="@if($user->group_id==1) uk-text-danger @endif">{{ $user->email }}</a></b></div>
                                @if($user->id>1 )
                                <div class="uk-width-auto">
                                    <a href="#delete" class="delete-link" data-id="{{ $user->id }}" data-title="ID: {{ $user->id }} / Title: {{ $user->email }}" data-uk-toggle><span data-uk-icon="icon: trash" class=""></span></a>
                                </div>
                                @endif
                                <div class="uk-width-1-1" id="user-{{ $user->id }}" hidden>
                                    <div data-uk-grid class="uk-grid-small">
                                        <div class="uk-width-1-1"><hr></div>
                                        <div class="uk-width-expand uk-flex uk-flex-middle"><b>{{ __('admin.userGroup') }}</b>: {{ $groups[$user->group_id] }}</div>
                                        @if($user->id>1 )
                                        <div class="uk-width-auto uk-flex uk-flex-middle">
                                            <form method="POST" action="{{ route('changeGroup') }}">
                                                @csrf
                                                @foreach($groups as $key=>$value)
                                                    <label class="uk-margin-small-right">
                                                        <input type="radio" name="group" value="{{ $key }}" @if($user->group_id == $key) checked @endif >
                                                        {{ $value }}
                                                    </label>
                                                @endforeach

                                                <input type="text" value="{{ $user->id }}" name="id" required hidden>
                                                <button type="submit" class="uk-button uk-button-primary uk-button-small">{{ __('admin.changeGroupButton') }}</button>
                                            </form>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="uk-width-1-1">
            {{ $users->links('pagination') }}
        </div>
    </div>
    <div id="delete" class="uk-flex-top" data-uk-modal>
        <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div data-uk-grid class="uk-grid-small">
                <div class="uk-width-1-1"><h3 class="uk-text-danger uk-text-bold">{{ __('admin.attention') }}</h3></div>
                <div class="uk-width-1-1">{{ __('admin.modalDeleteDesc') }}</div>
                <div class="uk-width-1-1 uk-text-bold"><span class="delete-title"></span></div>
                <div class="uk-width-expand uk-flex uk-flex-right">
                    <button class="uk-button uk-button-default uk-modal-close" type="button">{{ __('admin.noButton') }}</button>
                </div>
                <div class="uk-width-auto">
                    <form method="POST" action="{{ route('admin.user.destroy', 0) }}" class="delete-form">
                        @csrf
                        @method('DELETE')
                        <input type="text" value="1" name="id" class="delete-id" required hidden>
                        <button type="submit" class="uk-button uk-button-primary">{{ __('admin.yesButton') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
