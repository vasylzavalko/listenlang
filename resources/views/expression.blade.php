@extends('layouts.home')

@section('content')
    <div data-uk-height-viewport="expand: true" class="uk-section uk-section-small uk-section-muted uk-flex uk-flex-center uk-flex-middle uk-position-relative">
        <div class="expression-back"><a href="{{ route('home') }}"><span data-uk-icon="icon: arrow-left" class="uk-margin-small-right"></span>{{ __('home.categoryChange') }}</a></div>
        <div class="expression-title"><h1>{{ $category['title'] }}</h1></div>
        <div class="expression-count">{{ $from  }} - {{ $to  }}</div>
        <div class="expression-control">
            <a href="" class="button button-play" data-id="{{ $category['id'] }}"></a>
            <a href="" class="button button-pause uk-hidden" data-id="{{ $category['id'] }}"></a>
            <a href="" class="button button-restart uk-hidden" data-id="{{ $category['id'] }}"></a>
        </div>
        <div class="expression-time"><span></span></div>
        <div class="expression">
            <ul class="expression-items">
                @foreach($expressions as $value)
                <li class="@if ($loop->first) active @endif expression-{{ $value['id'] }}" data-id="{{ $value['id'] }}" data-fromvoice="{{ $value['from_voice'] }}" data-tovoice="{{ $value['to_voice'] }}">
                    <div class="expression-title-from">{{ $value['from_title'] }}</div>
                    <div class="divider"></div>
                    <div class="expression-title-to">{{ $value['to_title'] }}</div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
