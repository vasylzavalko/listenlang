@extends('layouts.home')

@section('content')
    <div data-uk-height-viewport="expand: true" class="uk-section uk-section-small uk-section-muted uk-flex uk-flex-center uk-flex-middle">
        <div class="home-category">
            <div data-uk-grid class="uk-grid-small">
                <div class="uk-width-1-1">
                    <div class="welcome">
                        <h1>{{ __('home.homeTitle') }}</h1>
                        <p>{{ __('home.homeContent') }}</p>
                    </div>
                </div>
                <div class="uk-width-1-1 uk-flex uk-flex-center">
                    <div class="select-lang-box">
                        <div class="select-langs uk-inline">
                            <span data-uk-icon="icon: chevron-down" class="button"></span>
                            <a href="#" class="select-lang-title from">{{ $langs[$from]['title'] }}</a>
                            <div data-uk-dropdown="mode: click; pos: bottom-center">
                                <ul>
                                    @foreach($langs as $value)
                                        <li class="select-lang" data-type="from" data-id="{{ $value['id'] }}" data-title="{{ $value['title'] }}">{{ $value['title'] }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="reverse reverse-line"><span></span></div>
                        <div class="select-langs uk-inline">
                            <span data-uk-icon="icon: chevron-down" class="button"></span>
                            <a href="#" class="select-lang-title to">{{ $langs[$to]['title'] }}</a>
                            <div data-uk-dropdown="mode: click; pos: bottom-center">
                                <ul class="select-lang-to">
                                    @foreach($langs as $value)
                                        <li class="select-lang to-{{ $value['id'] }} @if($value['id']==$from) uk-hidden @endif" data-type="to" data-id="{{ $value['id'] }}" data-title="{{ $value['title'] }}">{{ $value['title'] }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-1 uk-margin-medium-top">
                    <div data-uk-grid class="uk-grid-small" data-uk-height-match="target: .box-height">
                        @foreach($categories as $value)
                        <div class="uk-width-1-2@s uk-width-1-3@m uk-width-1-4@l">
                            <div class="category-box box-height uk-transition-toggle">
                                <div class="category-box-padding">
                                    <div data-uk-grid class="uk-grid-small">
                                        <div class="uk-width-1-1">
                                            <div class="category-box-header">{{ __('home.expressionsCount') }}: {{ $value['expressions'] }}</div>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <a href="{{ route('expression') }}?c={{ $value['id'] }}" class="uk-inline-clip">
                                                <div class="uk-cover-container uk-transition-scale-up uk-transition-opaque">
                                                    <canvas width="600" height="300"></canvas>
                                                    <img src="{{ $value['image'] }}" alt="{{ $value['title'] }}" data-uk-cover>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <a href="{{ route('expression') }}?c={{ $value['id'] }}" class="category-box-link">{{ $value['title'] }}</a>
                                        </div>
                                        @if(isset($value['childs']))
                                        <div class="uk-width-1-1">
                                            <ul class="category-box-ul">
                                                @foreach($value['childs'] as $valueChild)
                                                    <li><a href="{{ route('expression') }}?c={{ $valueChild['id'] }}">{{ $valueChild['title'] }}</a><span>({{ $valueChild['expressions'] }})</span></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (session('message'))
        <div class="message">{{ __('home.alertMessage'.session('message')) }}</div>
    @endif
@endsection
