@extends('layouts.home')

@section('content')
    <div data-uk-height-viewport="expand: true" class="uk-section uk-section-small uk-section-muted uk-flex uk-flex-center uk-flex-middle">
        <div class="home-category">
            <div data-uk-grid class="uk-grid-small">
                <div class="uk-width-1-1">
                    <div class="about">
                        <h1>{{ __('home.aboutTitle') }}</h1>
                        <p>{{ __('home.aboutContent') }}</p>
                        <a href="#" class="uk-button uk-button-primary install-button">{{ __('home.installPWA') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
