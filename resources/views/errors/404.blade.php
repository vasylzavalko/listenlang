@extends('layouts.error')

@section('content')
    <div data-uk-height-viewport="expand: true" class="uk-section uk-section-small uk-section-muted uk-flex uk-flex-center uk-flex-middle">
        <div class="error-404">
            <h1>404 Error: Page or Document not found</h1>
            This could be caused by the following:
            <ul>
                <li>The page or document is temporarily not available</li>
                <li>The page or document was renamed and no longer exists</li>
                <li>The bookmark being used is outdated</li>
                <li>The URL was entered incorrectly</li>
            </ul>
            <p>Please return to <a href="{{ route('home') }}">ListenLang</a></p>
        </div>
    </div>
@endsection
