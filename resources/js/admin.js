UIkit.util.on('#sorting', 'moved', function () {
    var children 	= this.children;
    var num  		= this.children.length;
    var sort		= [ ];
    for (var i = 0; i < num; i++) {
        sort.push(children[i].id);
    }
    var sort = sort.join('|');

    var table = $('#sorting').data('table');

    $.ajax({
        url:"/admin/action/sorting",
        type:"POST",
        data:{
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "sort": sort,
            "table": table
        },
        success:function(data) {
            if(data){
                console.log("ok");
            } else {
                console.log("no");
            }
        }
    });
});

$(".delete-link").click(function(event) {
    event.preventDefault();
    $('.delete-title').text($(this).data('title'));
    $('.delete-id').val($(this).data('id'));
});

$(".delete-image").click(function(event) {
    event.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url:"/admin/action/deleteCategoryImage",
        type:"POST",
        data:{
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "id": id
        },
        success:function(data) {
                console.log(data);
        }
    });
});

$(".select-lang").click(function(event) {
    event.preventDefault();
    var id = $(this).data('id');
    var title = $(this).data('title');
    var type = $(this).data('type');
    $('.'+type).text(title);
    selectlang(type, id);
    if(type=='from') {
        $('.select-lang').removeClass('uk-hidden');
        $('.to-' + id).addClass('uk-hidden');
        var to = $('.select-lang-to li:not(.uk-hidden):first');
        $('.to').text(to.data('title'));
    }
    UIkit.toggle('.'+type).toggle();
});

var second = 5000;
var expressionCount = $('.expression-items').find('li').length;
var expressionLast = expressionCount-1;
var expressionId = [];
$('.expression-items li').each(function() {
    expressionId.push($(this).data('id'));
});
$('.expression-count').text(expressionCount+' / 1');

var audioFrom = document.createElement('audio');
var audioTo = document.createElement('audio');

$(".button-play").click(function(e) {
    e.preventDefault();
    var intervals = [];
    $('.button-play').addClass('uk-hidden');
    $('.button-pause').removeClass('uk-hidden');

    for( let i = 0; i<expressionCount; i++ ){

        var timer = setTimeout(function() {
            var id = expressionId[i];
            var count = i+1;

            $('.expression-items li').removeClass('active');
            $('.expression-'+id).addClass('active');
            play(id);

            $('.expression-count').text(expressionCount+' / ' + count );
//            console.log(i + ' - ' + count);

            if( count == expressionCount ){
                var timer = setTimeout(function() {
                    $(".button-play").click();
                }, second);
            }
            $('.expression-time span').animate({width: "100%"}, 5000);
            $('.expression-time span').animate({width: "0%"}, 10);

        }, i * second);
        intervals.push(timer);

        $(".button-pause").click(function(e) {
            e.preventDefault();
            intervals.forEach(clearInterval);
            i = expressionCount;
            $('.button-play').removeClass('uk-hidden');
            $('.button-pause').addClass('uk-hidden');
        });
    }
});

function play(id) {
    var fromVoice = $('.expression-'+id).data('fromvoice');
    var toVoice = $('.expression-'+id).data('tovoice');
    audioFrom.setAttribute('src', '/files/voice/'+id+'/'+fromVoice);
    audioTo.setAttribute('src', '/files/voice/'+id+'/'+toVoice);
    $('.expression-title-from').addClass( "active" );
    $('.expression-title-to').removeClass( "active" );
    audioFrom.play();
    audioFrom.addEventListener('ended', function() {
        $('.expression-title-to').addClass( "active" );
        $('.expression-title-from').removeClass( "active" );
        audioTo.play();
    }, false);
    //    duration = audioFrom.duration;
}


function selectlang(type, id) {
    $.ajax({
        url:"/action/selectLang",
        type:"POST",
        data:{
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "id": id,
            "type": type,
        },
        success:function(data) {
            console.log(data);
            return data;
        }
    });
}

function expressionJson(id){
    $.ajax({
        url:"/expressionJson?c="+id,
        success:function(data) {
            console.log(data);
        }
    });
}

$(".tag-link").click(function(event) {
    event.preventDefault();
    var expression = $(this).data('expression');
    var lang = $(this).data('lang');
    var tag = $(this).data('tag');
    var action = ($(this).hasClass('active'))?"delete":"add";
    $.ajax({
        url:"/admin/action/tag",
        type:"POST",
        data:{
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "action": action,
            "expression_id": expression,
            "lang_id": lang,
            "tag": tag,
        },
        success:function(data) {
            if(data){
                $(this).toggleClass('active');
            }
        }
    });
    $(this).toggleClass('active');
});

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js', {
        scope: '.'
    }).then(function (registration) {
        // Registration was successful
        console.log('Laravel PWA: ServiceWorker registration successful with scope: ', registration.scope);
    }, function (err) {
        // registration failed :(
        console.log('Laravel PWA: ServiceWorker registration failed: ', err);
    });
}

let deferredPrompt; // Allows to show the install prompt
const installButton = document.getElementById("install_button");

window.addEventListener("beforeinstallprompt", e => {
    console.log("beforeinstallprompt fired");
    // Prevent Chrome 76 and earlier from automatically showing a prompt
    e.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = e;
    // Show the install button
    //installButton.hidden = false;
    console.log("ok");
    $('.install-button').click(function() {
        console.log("ok");
        installApp();
    });
});

function installApp() {
    // Show the prompt
    deferredPrompt.prompt();
    //installButton.disabled = true;

    // Wait for the user to respond to the prompt
    deferredPrompt.userChoice.then(choiceResult => {
        if (choiceResult.outcome === "accepted") {
            console.log("PWA setup accepted");
            $('#install_button').css("display", "none");
           // installButton.hidden = true;
        } else {
            console.log("PWA setup rejected");
        }
        //installButton.disabled = false;
        deferredPrompt = null;
    });
}

window.addEventListener("appinstalled", evt => {
    console.log("appinstalled fired", evt);
});
