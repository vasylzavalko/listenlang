<?php

return [

    'uk' => 'UA',
    'ru' => 'RU',
    'en' => 'EN',
    'login' => 'Увійти',
    'logout' => 'Вийти',
    'register' => 'Зареєструватись',
    'adminpanel' => 'Адмін-панель',

    'loginTitle' => 'Авторизація', // Log In
    'name' => 'Ім\'я',
    'email' => 'Email',
    'emailAddress' => 'Email', // E-Mail Address
    'password' => 'Пароль', // Password
    'сonfirmPassword' => 'Повторити пароль', // Confirm Password
    'rememberMe' => 'Запам\'ятати мене', // Remember Me
    'loginButton' => 'Увійти', // Log In
    'forgotYourPassword' => 'Забули пароль?', // Forgot Your Password?

    'resetPasswordTitle' => 'Відновити пароль', // Reset Password
    'sendPasswordResetLinkButton' => 'Відновити', // Send Password Reset Link

    'registerTitle' => 'Реєстрація', // Register
    'registerButton' => 'Зареєструватись', // Send Password Reset Link

    'pwaInstallDesc' => 'Встановити додаток ListenLang.',
    'pwaInstallButton' => 'Встановити',
    'aboutApp' => 'Про додаток',

    'expressionsCount' => 'Фраз',
    'categoryChange' => 'Змінити категорію',
    'homeTitle' => 'Вивчай мову за фразами!',
    'homeContent' => 'Зазначте мови із якої в яку перекладати та оберіть категорію фраз.',
    'startStudy' => 'Розпочати навчання',
    'aboutTitle' => 'Додаток вивчення мови.',
    'aboutContent' => 'Додаток ListenLang для тих, хто вивчає іноземну мову. Допоможе поліпшити словниковий запас за допомогою карток поширених розмовних фраз.',
    'installPWA' => 'Встановити додаток',

    'alertMessageCategoryClearSelectOther' => 'Обрана вами категорія порожня. Виберіть іншу!',

];
