<?php
return [
    'expression' => 'Фраза',
    'expressions' => 'Фрази',
    'user' => 'Користувач',
    'users' => 'Користувачі',
    'translate' => 'Переклад',
    'translates' => 'Переклади',
    'category' => 'Категорія',
    'categories' => 'Категорії',
    'tag' => 'Тег',
    'tags' => 'Теги',
    'lang' => 'Мова',
    'langs' => 'Мови',
    'attention' => 'Увага',
    'backToSite' => 'Повернутись на сайт',
    'noEntries' => 'Записи відсутні',
    'editTitle' => 'Редагування',
    'status' => 'Статус',
    'saveButton' => 'Зберегти',
    'saveExitButton' => 'Зберегти та вийти',
    'addButton' => 'Додати',
    'updateButton' => 'Оновити',
    'deleteButton' => 'Видалити',
    'cancelButton' => 'Скасувати',
    'yesButton' => 'Так',
    'noButton' => 'Ні',
    'viewAllCategory' => 'Показати всі категорії',
    'allCategory' => 'Всі категорії',

    'langsTitle' => 'Мови',
    'langsAddTitle' => 'Додати нову мову',
    'addLangsButton' => 'Додати мову',
    'langTitle' => 'Назва',
    'langCode' => 'Код мови, локаль',
    'langLocales' => 'Довідник локалей',
    'langVoiceCode' => 'Код голосу',
    'langVoiceName' => 'Назва голосу',

    'categoriesTitle' => 'Категорії',
    'categoryAddTitle' => 'Додати нову категорію',
    'addCategoryButton' => 'Додати категорію',
    'categoryTitle' => 'Назва категорії',
    'categorySlug' => 'URL категорії (SLUG)',

    'usersTitle' => 'Користувачі',
    'userGroup' => 'Група користувача',
    'changeGroupButton' => 'Оновити групу',

    'expressionsTitle' => 'Фрази',
    'expressionAddTitle' => 'Додати нову фразу',
    'addExpressionButton' => 'Додати фразу',
    'expressionTitle' => 'Фраза',
    'noExpression' => 'Фрази відсутні',

    'tagsTitle' => 'Теги',
    'noTags' => 'Теги відсутні',

    'image' => 'Зображення',
    'imageSelect' => 'Вибрати зображення',
    'imageDelete' => 'Видалити зображення',

    'alertMessageLangAddSuccessful' => 'Ви успішно додали нову мову.',
    'alertMessageLangDeleteSuccessful' => 'Ви успішно видалили мову.',
    'alertMessageLangUpdateSuccessful' => 'Ви успішно оновили мову.',
    'alertMessageCategoryAddSuccessful' => 'Ви успішно додали нову категорію.',
    'alertMessageCategoryDeleteSuccessful' => 'Ви успішно видалили категорію.',
    'alertMessageCategoryUpdateSuccessful' => 'Ви успішно оновили категорію.',
    'alertMessageCategoryDeleteNotEmpty' => 'Категорія не видалена! Видаляти можна лише категорії без підкатегорій.',

    'alertMessageExpressionAddSuccessful' => 'Ви успішно додали нову фразу.',
    'alertMessageExpressionDeleteSuccessful' => 'Ви успішно видалили запис.',
    'alertMessageExpressionUpdateSuccessful' => 'Ви успішно оновили фразу.',

    'alertMessageUserUpdateSuccessful' => 'Ви успішно оновили дані користувача.',
    'alertMessageUserDeleteSuccessful' => 'Ви успішно видалили користувача.',

    'modalDeleteDesc' => 'Ви підтверджуєте видалення запису?',

    'backToLangs' => 'Повернутись до переліку мов',
    'backToCategory' => 'Повернутись до переліку категорій',
    'backToCategoryUp' => 'Перейти вверх по категорії',
    'backToExpression' => 'Повернутись до переліку фраз',


];
