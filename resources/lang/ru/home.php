<?php

return [

    'uk' => 'UA',
    'ru' => 'RU',
    'en' => 'EN',
    'login' => 'Войти',
    'logout' => 'Выйти',
    'register' => 'Регистрация',
    'adminpanel' => 'Админ-панель',

    'loginTitle' => 'Авторизация', // Log In
    'name' => 'Имя',
    'email' => 'Email',
    'emailAddress' => 'Email', // E-Mail Address
    'password' => 'Пароль', // Password
    'сonfirmPassword' => 'Повторить пароль', // Confirm Password
    'rememberMe' => 'Запомнить меня', // Remember Me
    'loginButton' => 'Войти', // Log In
    'forgotYourPassword' => 'Забыли пароль?', // Forgot Your Password?

    'resetPasswordTitle' => 'Восстановить пароль', // Reset Password
    'sendPasswordResetLinkButton' => 'Восстановить', // Send Password Reset Link

    'registerTitle' => 'Регистрация', // Register
    'registerButton' => 'Регистрация', // Send Password Reset Link

    'pwaInstallDesc' => 'Установить приложение ListenLang.',
    'pwaInstallButton' => 'Установить',
    'aboutApp' => 'О приложении',

    'expressionsCount' => 'Фраз',
    'categoryChange' => 'Выбрать категорию',
    'homeTitle' => 'Изучай язык по фразам!',
    'homeContent' => 'Укажите языки с какого в какой переводить и выберите категорию фраз.',
    'startStudy' => 'Начать обучение',
    'aboutTitle' => 'Приложение по изучению языка.',
    'aboutContent' => 'Приложение ListenLang для изучающих иностранный язык. Поможет улучшить словарный запас с помощью карточек распространенных разговорных фраз.',
    'installPWA' => 'Установить приложение',

    'alertMessageCategoryClearSelectOther' => 'Выбранная вами категория пуста. Выберите другую!',

];
