<?php

return [

    'uk' => 'UA',
    'ru' => 'RU',
    'en' => 'EN',
    'login' => 'Login',
    'logout' => 'Logout',
    'register' => 'Register',
    'adminpanel' => 'Admin panel',

    'loginTitle' => 'Log In',
    'name' => 'Name',
    'email' => 'Email',
    'emailAddress' => 'E-Mail Address',
    'password' => 'Password',
    'сonfirmPassword' => 'Confirm Password',
    'rememberMe' => 'Remember Me',
    'loginButton' => 'Log In',
    'forgotYourPassword' => 'Forgot Your Password?',

    'resetPasswordTitle' => 'Reset Password',
    'sendPasswordResetLinkButton' => 'Send Password Reset Link',

    'registerTitle' => 'Register',
    'registerButton' => 'Register',

    'pwaInstallDesc' => 'Install ListenLang application.',
    'pwaInstallButton' => 'Install',
    'aboutApp' => 'About Front',

    'expressionsCount' => 'Expression',
    'categoryChange' => 'Change Category',
    'homeTitle' => 'Learn the language by phrases!',
    'homeContent' => 'Specify the languages from which to translate and select the category of phrases.',
    'startStudy' => 'Start Study',
    'aboutTitle' => 'Language learning application.',
    'aboutContent' => 'ListenLang application for those who are learning a foreign language. It will help to improve vocabulary with the help of cards of common colloquial phrases.',
    'installPWA' => 'Install the Front',

    'alertMessageCategoryClearSelectOther' => 'The category you selected is empty. Choose another!',

];
