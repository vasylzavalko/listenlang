<?php

use App\Providers\RouteServiceProvider;
use Laravel\Fortify\Features;

return [

    'status' => 1,
    'apiKey' => 'AIzaSyCZ6T1BFhSIk36Ip_9jcUK9N3COLU7OyX4',
    'apiUrlTranslate' => 'https://www.googleapis.com/language/translate/v2',
    'apiUrlTextToSpeech' => 'https://texttospeech.googleapis.com/v1beta1/text:synthesize',
    'speakingRate' => 0.8,
];
