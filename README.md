# ListenLang

**Сервіс вивчення іноземних мов за типом "слухай, повторюй, вивчай".**

Користувач вибирає мови з якої в яку перекладати, а також обирає потрібну категорію чи тег. Підбирається масив фраз які виводяться одна за одною разом із перекладом. Окрім текстової частини кожна фраза під час виводу озвучується.


----

[Технічне завдання](https://gitlab.com/vasylzavalko/listenlang/-/blob/main/technicaltask.md) 